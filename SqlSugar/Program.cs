﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace SqlSugarDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var defaultContext = new DefaultContext("Server =.; DataBase = StuScript; Uid = sa; Pwd = lmz8816766", DbType.SqlServer);
            //defaultContext.Client.CodeFirst.InitTables<Person>();
            //for (int i = 0; i < 30; i++)
            //{
            //    defaultContext.Client.Insertable<Person>(new Person() { Name = $"张{i}明", Age = 25 }).ExecuteCommand();
            //}
            //var list = (from a in defaultContext.Client.Queryable<Person>() select a).ToList();
            var list = defaultContext.Client.Queryable<Person>().ToList();
            list.ForEach(person =>
            {
                Console.WriteLine($"学号={person.Id}，姓名={person.Name}，年龄={person.Age}");
            });
            list[0].Name = "李大江";
            defaultContext.Client.Updateable<Person>(list).ExecuteCommand();
            //wait
            Console.Read();
        }
    }
}
