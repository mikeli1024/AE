﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlSugarDemo
{
    public class DefaultContext
    {
        public SqlSugarClient Client { get; }
        public DefaultContext(string connstring,DbType dbType)
        {
            Client = new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = connstring,
                DbType = dbType,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.Attribute
            }) ;
            
        }
    }
}
