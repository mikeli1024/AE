﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;

namespace AE.Common.MVVMCore
{
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;
        public DelegateCommand(Action<object> execute,Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException("execute can not be null");
            _canExecute = canExecute;
        }
        public DelegateCommand(Action<object> execute):this(execute,null){}
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke(parameter)?? true;
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
