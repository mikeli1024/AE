﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace AE.Common.MVVMCore
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// 触发属性改变事件
        /// </summary>
        /// <param name="propertyName">属性名</param>
        protected void RaisePropertyChanged([CallerMemberName] string propertyName=null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        /// <summary>
        /// 设置值，确保属性值发生了变化再执行RaisePropertyChanged方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="target">目标属性</param>
        /// <param name="value">设置值</param>
        /// <param name="propertyName">属性名</param>
        /// <returns></returns>
        protected bool SetProperty<T>(ref T target,T value,[CallerMemberName] string propertyName=null)
        {
            if (EqualityComparer<T>.Default.Equals(target, value)) return false;
            target = value;
            RaisePropertyChanged(propertyName);
            return true;
        }
    }
}
