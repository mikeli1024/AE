﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;
using System.Text;
using System.Threading.Tasks;

namespace AE.Common
{
    public class Logger
    {
        private static Logger _instance;
        private static readonly object _lock = new object();
        public static Logger Instance
        {
            get
            {
                if (_instance==null)
                {
                    lock(_lock)
                    {
                        if (_instance==null)
                        {
                            _instance = new Logger();
                        }
                    }
                }
                return _instance; 
            }
        }
        private Logger()
        {
            ConfigLog();
        }
        private void ConfigLog()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            var xml = assembly.GetManifestResourceStream("AE.Common.CommonConfigs.log4net.xml");
            log4net.Config.XmlConfigurator.Configure(xml);
            _logDebug= LogManager.GetLogger("LogDebug");
            _logInfo = LogManager.GetLogger("LogInfo");
            _logWarn = LogManager.GetLogger("LogWarn");
            _logError = LogManager.GetLogger("LogError");
        }
        private  ILog _logDebug = null;
        private  ILog _logInfo = null;
        private  ILog _logWarn = null;
        private  ILog _logError = null;
        public  void Debug(string mes)
        {
            _logDebug.Debug(mes);
        }
        public  void Info(string mes)
        {
            _logInfo.Info(mes);
        }
        public  void Warn(string mes)
        {
            _logWarn.Warn(mes);
        }
        public  void Error(string mes)
        {
            _logError.Error(mes);
        }
        public  void Fatal(string mes)
        {
            _logError.Fatal(mes);
        }
    }
}
