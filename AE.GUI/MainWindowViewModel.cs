﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AE.Common.MVVMCore;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AE.GUI
{
    public class MainWindowViewModel:ViewModelBase
    {
        public MainWindowViewModel()
        {
            Region = new Views.StudentManage();
            SearchCmd = new DelegateCommand<bool>(isChecked => { BackColor = isChecked ? "Red" : "Blue"; });
        }
        private ContentControl _region;
        public ContentControl Region
        {
            get => _region;
            set => SetProperty(ref _region, value);
        }

        public DelegateCommand<bool> SearchCmd { get; set; }
        private string _backColor;

        public string BackColor
        {
            get => _backColor;
            set => SetProperty(ref _backColor, value);
        }

    }
}
