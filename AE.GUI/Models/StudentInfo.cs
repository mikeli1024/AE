﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AE.Common.MVVMCore;

namespace AE.GUI.Models
{
    public class StudentInfo
    {
        public int StuId { get; set; }
        public string StuName { get; set; }
        public int StuClassId { get; set; }
    }
}
