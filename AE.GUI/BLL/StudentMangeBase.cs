﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AE.Common.SimpleORM;
using AE.Common;
using AE.GUI.Models;

namespace AE.GUI.BLL
{
    public class StudentMangeBase
    {
        private OrmUtil<StudentInfo> _ormUtil;
        public List<StudentInfo> AllStudents { get; private set; }
        public StudentMangeBase()
        {
            _ormUtil = new OrmUtil<StudentInfo>();
            _ormUtil.Connection = new System.Data.SqlClient.SqlConnection("Server=.;DataBase=StuScript;Uid=sa;Pwd=lmz8816766");
            GetAllStudents();
        }
        private void GetAllStudents()
        {
            try
            {
                AllStudents = _ormUtil.Search(string.Empty);
            }
            catch (Exception ex)
            {
                Logger.Instance.Error("读取数据库发生错误" + ex.Message);
            }
        }
        public IEnumerable<StudentInfo> GetStudentsById(string id)
        {
            try
            {
                //var sql = $" where StuId = {id}";
                if (id==string.Empty)
                {
                    return AllStudents;
                }
                return AllStudents.Where(stu => stu.StuId.ToString().Contains(id));
            }
            catch (Exception)
            {

                
            }
            return null;
        }

    }
}
