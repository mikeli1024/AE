﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using AE.Common.MVVMCore;
using AE.GUI.BLL;
using AE.GUI.Models;


namespace AE.GUI.ViewModels
{
    public class StudentManageViewModel:ViewModelBase
    {
        private StudentMangeBase _base;
        private ObservableCollection<StudentInfo> _students;
        public ObservableCollection<StudentInfo> Students
        {
            get => _students;
            set => SetProperty(ref _students, value);
        }
        public DelegateCommand<string> QueryStuCmd { get; private set; }
        public DelegateCommand<int> QueryStuByIdCmd { get; private set; }
        public DelegateCommand<string> DeleteStuCmd { get; private set; }
        public StudentManageViewModel()
        {
            _base = new StudentMangeBase();//业务层对象
            Students = new ObservableCollection<StudentInfo>(_base.AllStudents);
            QueryStuCmd = new DelegateCommand<string>(stuId =>
              {
                  Students = new ObservableCollection<StudentInfo>(_base.GetStudentsById(stuId));
              });
            QueryStuByIdCmd = new DelegateCommand<int>(stuId =>
              {
               Students = new ObservableCollection<StudentInfo>(_base.GetStudentsById(stuId.ToString()));
              });
        }


    }
}
