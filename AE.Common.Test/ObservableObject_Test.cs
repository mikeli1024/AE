﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AE.Common.MVVMCore;

namespace AE.Common.Test
{
    [TestClass]
    public class ObservableObject_Test
    {
        /// <summary>
        /// 初始化测试
        /// </summary>
        [TestInitialize]
        public void EnableLogger()
        {
            Logger.Instance.Debug($"{typeof(ObservableObject).Name}开始测试");
        }
        /// <summary>
        /// 抽象类确认
        /// </summary>
        [TestMethod]
        public void IsAbstract()
        {
            var type = typeof(ObservableObject);
            Assert.IsTrue(type.IsAbstract);
        }
        /// <summary>
        /// 测试类
        /// </summary>
        [TestClass]
        public class ObservableObjectSample:ObservableObject
        {
            private int _propertySample;

            public int PropertySample
            {
                get => _propertySample;
                set => SetProperty(ref _propertySample, value);
            }
        }
        [TestMethod]
        public void CanRaisePropertyChanged()
        {
            bool isRaisePropertyChanged = false;
            ObservableObjectSample sample = new ObservableObjectSample();
            sample.PropertyChanged += (s, e) =>
            {
                isRaisePropertyChanged = true;
                Logger.Instance.Debug($"PropertyName:{e.PropertyName}");
            };
            sample.PropertySample = 666;
            Assert.IsTrue(isRaisePropertyChanged);
            Assert.AreEqual(sample.PropertySample, 666);
        }
        [TestMethod]
        public void WhenPropertyValueEqualsOldValue_NotRaise()
        {
            bool isRaisePropertyChanged = false;
            ObservableObjectSample sample = new ObservableObjectSample() { PropertySample = 666 };
            sample.PropertyChanged += (s, e) =>
              {
                  isRaisePropertyChanged = true;
                  Logger.Instance.Debug($"PropertyName:{e.PropertyName}");
              };
            Assert.IsFalse(isRaisePropertyChanged);
            Assert.AreEqual(sample.PropertySample, 666);
        }
        [TestCleanup]
        public void CleanThisTest()
        {
            Logger.Instance.Debug($"{typeof(ObservableObject).Name}测试结束");
        }
    }
}
