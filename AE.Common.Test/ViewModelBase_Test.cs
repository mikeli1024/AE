﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AE.Common.MVVMCore;

namespace AE.Common.Test
{
    /// <summary>
    /// 测试触发依赖项改变
    /// </summary>
    [TestClass]
    public class ViewModelBase_Test
    {
        /// <summary>
        /// 初始化测试
        /// </summary>
        [TestInitialize]
        public void EnableLogger()
        {
            Logger.Instance.Debug($"{nameof(ViewModelBase)}开始测试");            
        }
        [TestClass]
        public class People:ViewModelBase
        {
            private string _firstName="None";

            public string FirstName
            {
                get => _firstName;
                set { if (SetProperty(ref _firstName, value)) RaisePropertyChanged(nameof(FullName)); }
            }
            private string _lastName="None";

            public string LastName
            {
                get => _lastName;
                set { if (SetProperty(ref _lastName, value)) RaisePropertyChanged(nameof(FullName)); }
            }
            public string FullName => $"{FirstName}-{LastName}";
        }
        [TestMethod]
        public void CanRaiseByOther()
        {
            People people = new People();
            bool isRaise = false;
            people.PropertyChanged += (s, e) =>
              {
                  if (e.PropertyName==nameof(People.FullName))
                  {
                      isRaise = true;
                      Logger.Instance.Debug($"FullName Changed To{people.FullName}");
                  }
              };
            people.FirstName = "Jam";
            people.LastName = "James";
            Assert.IsTrue(isRaise);
        }

    }
}
